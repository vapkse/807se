/*
807SE
 */
#include <SimpleTimer.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <SmoothAnalogInput.h>
#include <Blink.h>

static const byte SE807_ID = 207;
static const byte ampId = SE807_ID;

// Pin Config
#define ledPin			2 
#define relayPin		10 
#define regulatorRefPin		3 
#define indicatorPin		5 
#define aopFlag1		6  // Future usage
#define aopFlag2		7  // Future usage
#define leftCurrentPin		A1     
#define rightCurrentPin		A2     
#define switchPin		A3     
#define airTemperaturePin       A6

// Constants
unsigned int biasRef = 160;	            // 0(0V) to 255(5V)
unsigned int startCurrent = 30;	            // 0.0049V per Units
unsigned int minCurrent = 350;	            // 0.0049V per Units
unsigned int maxCurrent = 550;	            // 0.0049V per Units
unsigned int switchOffset = 122; 	    // 0.0049V per Units (0.6V)
unsigned int dischargeMaxTime = 10;	    // seconds.
unsigned int heatMaxTime = 45;	            // seconds.
unsigned int highVoltageMaxTime	= 2;	    // seconds.
unsigned int stabilizationMaxTime = 8;      // seconds.
float indicatorRatio = 0.127;

// Internal use
SimpleTimer sendTimer;
SmoothAnalogInput LeftCurrentInput;
SmoothAnalogInput rightCurrentInput;
SmoothAnalogInput airTemperature;
Blink ledBlink;
unsigned int leftCurrentAverage;
unsigned int rightCurrentAverage;
unsigned int switchValue = 0;
unsigned int dischargeTime = 0;
unsigned long heatStartTime;
unsigned int heatTime = 0;
unsigned long highVoltageStartTime = 0;
unsigned int highVoltageTime = 0;
unsigned long stabilizationStartTime = 0;
unsigned int stabilizationTime = 0;
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;

// Errors
#define NO_ERR 0
#define ERR_DISHARGETOOLONG      2      // 2: Discharge too long
#define ERR_CURRENTONHEAT        3      // 3: Current during heat time
#define ERR_STABILIZINGTOOLONG   4      // 4: Stabilization too long
#define ERR_OUTOFRANGE           5      // 5: Out of range during normal function
byte errorNumber = NO_ERR;

#define NO_ERR_TUBE     0;
#define ERR_TUBE_LEFT   1;
#define ERR_TUBE_RIGHT  2;
byte errorTube = NO_ERR_TUBE;

// Sequence:
#define SEQ_DISCHARGE    0  // 0: Discharge
#define SEQ_HEAT         1  // 1: Heat tempo 
#define SEQ_STARTING     2  // 2: Starting High Voltage
#define SEQ_REGULATING   3  // 3: Waiting for reg
#define SEQ_FUNCTION     4  // 4: Normal Fonction
#define SEQ_FAIL         5  // 5: Fail
byte sequence = SEQ_DISCHARGE;

// Diagnostic
EasyTransfer dataTx; 
dataResponse dataTxStruct;

void sendDatas()
{    
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(leftCurrentAverage, 0, 1024, 0, 255); // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(rightCurrentAverage, 0, 1024, 0, 255); // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.temperature0 = constrain(airTemperature.read(), 0, 255); // 125deg max with LM35
  dataTxStruct.minValue = minCurrent >> 2;
  dataTxStruct.refValue = biasRef;
  dataTxStruct.maxValue = maxCurrent >> 2;
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorTube;
  dataTx.sendData();}

void RelayOn(void) 
{
  analogWrite(relayPin, 128);
}

void RelayOff(void) 
{
  analogWrite(relayPin, 0);
}

void ResetRegulators(void)
{
  analogWrite(regulatorRefPin, 0); 
}

void DisplayIndicator(void)
{
  int switchValue = analogRead(switchPin) + switchOffset/2;

  if (switchValue < switchOffset)
  {
    // Vu-meter read left channel
    analogWrite(indicatorPin, leftCurrentAverage * indicatorRatio);     
  }
  else if (switchValue < 2 * switchOffset)
  {
    // Vu-meter is off
    analogWrite(indicatorPin, 0); 
  }
  else
  {
    // Vu-meter read right channel
    analogWrite(indicatorPin, rightCurrentAverage * indicatorRatio);     
  }
}

boolean CheckInRange(unsigned int minValue, unsigned int maxValue)
{
  if (leftCurrentAverage < minValue || leftCurrentAverage > maxValue)
  {
    errorTube = ERR_TUBE_LEFT;
    return false;
  } 
  if (rightCurrentAverage < minValue || rightCurrentAverage > maxValue)
  {
    errorTube = ERR_TUBE_RIGHT;
    return false;
  }

  errorTube = NO_ERR_TUBE;
  return true;
}

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(aopFlag1, INPUT);     
  pinMode(aopFlag2, INPUT);     

  LeftCurrentInput.attach(leftCurrentPin, 32);
  rightCurrentInput.attach(rightCurrentPin, 32);
  airTemperature.attach(airTemperaturePin, 10);
  ledBlink.Setup(ledPin);
  sendTimer.setInterval(200, sendDatas);

  Serial.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);
}

// the loop routine runs over and over again forever:
void loop() 
{ 
  leftCurrentAverage = LeftCurrentInput.read();
  rightCurrentAverage = rightCurrentInput.read();

  // Diagnostic
  sendTimer.run();

  DisplayIndicator();

  switch (sequence)
  {  
  case SEQ_DISCHARGE:	
    ResetRegulators();
    RelayOff();
    ledBlink.Execute(800, 200);

    dischargeTime = millis() / 1000;
    if(dischargeTime > dischargeMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_DISHARGETOOLONG;
      break;
    }

    // Diagnostic
    stepMaxTime = dischargeMaxTime;
    stepElapsedTime = dischargeTime;
    stepMaxValue = 0;
    stepCurValue = 1;

    if(!CheckInRange(0, startCurrent))
    {
      break;
    }

    heatStartTime = millis();
    sequence++;

  case SEQ_HEAT: 
    if(!CheckInRange(0, startCurrent + 10))
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }
    ResetRegulators();
    RelayOff();
    ledBlink.Execute(400, 400);

    heatTime = (millis() - heatStartTime) / 1000;

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = heatTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = heatTime;

    if(heatTime < heatMaxTime)
    {
      break;
    }

    sequence++;
    ledBlink.On();
    delay(2500);  
    highVoltageStartTime = millis();

  case SEQ_STARTING:
    // Starting High Voltage
    ledBlink.Execute(100, 100);
    RelayOn();

    highVoltageTime = (millis() - highVoltageStartTime) / 1000;

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = highVoltageTime;
    stepMaxValue = highVoltageMaxTime;
    stepCurValue = highVoltageTime;

    if(highVoltageTime++ < highVoltageMaxTime)
    {
      break;  
    }  

    stabilizationStartTime = millis();
    sequence++;

  case SEQ_REGULATING: 
    // Waiting for regulator
    stabilizationTime = (millis() - stabilizationStartTime) / 1000;
    analogWrite(regulatorRefPin, biasRef); 

    // Diagnostic
    stepMaxTime = stabilizationMaxTime;
    stepElapsedTime = stabilizationTime;
    stepMaxValue = minCurrent + 10;
    stepCurValue = min(leftCurrentAverage, rightCurrentAverage);

    if(stabilizationTime > stabilizationMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_STABILIZINGTOOLONG;
      break;
    }

    ledBlink.Execute(20, 500);

    if(!CheckInRange(minCurrent + 10, maxCurrent))
    {
      break;
    }

    sequence++;

  case SEQ_FUNCTION:
    // Normal Fonction, wait and see        
    if(!CheckInRange(minCurrent, maxCurrent))
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_OUTOFRANGE;
      break;
    }

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;

    ledBlink.Off();
    break;

  default: 
    // Fail, protect mode
    ResetRegulators();
    RelayOff();
    ledBlink.Execute(250, errorNumber, 1200);

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;
  }  
}











